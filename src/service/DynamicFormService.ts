interface FormField {
    type: 'text' | 'number' | 'longtext' | 'dropdown'
    defaultValue?: string | number
    value?: string | number
    validation?: string
    min_value?: number
    max_value?: number
    options?: (string | number)[]
}
const formData: FormField[] = [
    {
        type: 'text',
        value: 'name',
        validation: '^\\s*$',
    },
    {
        type: 'number',
        value: 40,
        min_value: 30,
        max_value: 43
    },
    {
        type: 'longtext',
        value: 'Enter long text',
        validation: '^\\s*$',
    },
    {
        type: 'dropdown',
        options: ['Option 1', 'Option 2', 'Option 3'],
        value: 'Option 1',
    },
]
export const getFormData = (): FormField[] => {
    return formData
};