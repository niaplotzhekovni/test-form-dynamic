import React from 'react'
import FormInputs from './components/form/FormInputs'

function App() {
  return (
    <div>
      <FormInputs/>
    </div>
  )
}

export default App
