import React, {useEffect} from 'react'
import './input.css'

interface InputProps {
    label?: string;
    value?: string | number;
    type?: string;
    min?: number;
    max?: number;
    error?: string;
    onChange: (value: string, key: string) => void;
    defaultValue?: string | number;
    key: string;
}

const Input: React.FC<InputProps> = (props) => {
    useEffect(() => {
        if (props.defaultValue) {
          props.onChange(String(props.defaultValue), props.key)
        }
    }, [props.defaultValue])
    const inputCls = props.error && 'error';

    return (
        <>
            <div className="custom__input">
                {props.label && <label>{props.label}</label>}
                <input
                    value={String(props.value)}
                    onChange={(e) => props.onChange(e.target.value, props.key)}
                    className={inputCls || ''}
                    type={props.type}
                    min={props.min}
                    max={props.max}
                />
                {props.error && <span className="error__message">{props.error}</span>}
            </div>
        </>
    );
}

export default Input
