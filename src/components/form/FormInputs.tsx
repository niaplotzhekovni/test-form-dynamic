import React, {useState, useEffect} from 'react'
import './forminput.css'
import Input from '../inputs/Input'
import TextArea from '../textarea/TextArea'
import Select from '../select/Select'
import {getFormData} from "../../service/DynamicFormService";

interface FormField {
  type: 'text' | 'number' | 'longtext' | 'dropdown'
  defaultValue?: string | number
  value?: string | number
  validation?: string
  min_value?: number
  max_value?: number
  options?: (string | number)[]
  key?: any
}

const FormInputs = () => {
  const formData = getFormData()
  const formValuesWithKeys = formData.map((field, index) => ({
    ...field,
    key: `field_${index}`,
  }))

  const [formValues, setFormValues] = useState<FormField[]>(formValuesWithKeys)
  const [validationErrors, setValidationErrors] = useState<{[key: string]: string}>({})

  useEffect(() => {
    setFormValues(formValuesWithKeys)
  }, [])
  const validateForm = () => {
    const updatedValidationErrors: {[key: string]: string} = {}

    formValues.forEach((field) => {
      if (field.validation) {
        const regex = new RegExp(field.validation)
        if (regex.test(field.value as string)) {
          updatedValidationErrors[field.key as string] = 'invalid'
        }
      }
    })
    setValidationErrors(updatedValidationErrors)
    return Object.keys(updatedValidationErrors).length === 0
  }
  const handleInputChange = (value: string | number, fieldName: string) => {
    const updatedFormValues = formValues.map((field) => {
      if (field.key === fieldName) {
        return { ...field, value };
      }
      return field;
    });
    setFormValues(updatedFormValues);
  }

  const handleSubmit = (e: React.FormEvent) => {
    e.preventDefault()
    const isValid = validateForm()
    if (isValid) {
      alert('Push to server...')
    }
  }

  return (
    <div className="form-blocks">
      <form onSubmit={handleSubmit}>
        {formValues.map((field) => {
          const {type, defaultValue, value, options, key, min_value, max_value} = field
          if (type === 'text' || type === 'number') {
            return (
              <div className="form-item" key={key}>
                <Input
                  type={type}
                  key={key}
                  onChange={(e) => handleInputChange(e, key)}
                  defaultValue={defaultValue}
                  value={value}
                  min={min_value}
                  max={max_value}
                  error={validationErrors[key]}
                />
              </div>
            )
          } else if (type === 'longtext') {
            return (
              <div className="form-item" key={key}>
                <TextArea
                  defaultValue={defaultValue}
                  value={value}
                  onValueChange={(e) => handleInputChange(e, key)}
                  error={validationErrors[key]}
                />
              </div>
            )
          } else if (type === 'dropdown' && options && options.length > 0) {
            return (
              <div className="form-item" key={key}>
                <Select
                  value={value}
                  onValueChange={(selectedValue) => handleInputChange(selectedValue, key)} // Передаем selectedValue, а не событие e
                  options={options}
                />
              </div>
            )
          }
          return null
        })}
        <div className="form-button">
          <button className="button__base button__blue" type="submit">
            Send
          </button>
        </div>
      </form>
    </div>
  )
}

export default FormInputs
