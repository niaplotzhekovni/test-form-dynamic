import React, { useEffect, useState } from 'react';
import './select.css';
import ClickAwayListener from 'react-click-away-listener';

interface SelectProps {
  existValue?: boolean;
  value?: string | number;
  options: (string | number)[];
  onValueChange: (value: string | number) => void;
}

const Select: React.FC<SelectProps> = (props) => {
  const [isOpen, setIsOpen] = useState<boolean>(false);

  useEffect(() => {
    if (!props.existValue) {
      props.onValueChange(String(props.options[0]) || '');
    }
  }, []);

  const toggleOptions = () => {
    setIsOpen(!isOpen);
  };

  const selectOption = (item: string | number) => {
    props.onValueChange(item);
    setIsOpen(false);
  };

  return (
      <div className='custom-select-container'>
        <div className="select-wrapper">
          <div
              className='selected-option'
              onClick={toggleOptions}
          >
          <span className='selected-text'>
            {props.value}
          </span>
            <span
                className='selected-icon'
            ></span>
          </div>
          {isOpen && (
              <ClickAwayListener onClickAway={() => setIsOpen(false)}>
                <div className="select-options">
                  {props.options.map((el, index) => (
                      <div
                          key={index}
                          className="option"
                          onClick={() => selectOption(el)}
                      >
                        {el}
                      </div>
                  ))}
                </div>
              </ClickAwayListener>
          )}
        </div>
      </div>
  );
}

export default Select;
