import React, {useRef} from 'react'
import './textarea.css'

interface TextAreaProps {
  label?: string
  defaultValue?: string | number
  value?: string | number
  error?: string
  onValueChange: (value: string) => void
}

const TextArea: React.FC<TextAreaProps> = (props) => {
  const textArea = useRef<HTMLTextAreaElement | null>(null)

  const inputCls = props.error ? 'error' : ''

  return (
    <>
      <div className={`custom__textarea ${props.error ? 'has-error' : ''}`}>
        {props.label && <label className="custom__textarea__label">{props.label}</label>}
        <textarea
          rows={8}
          value={props.value || ''}
          className={inputCls || ''}
          onChange={(e) => props.onValueChange(e.target.value)}
          ref={textArea}
        />
      </div>
      {props.error && <span className="error__message">{props.error}</span>}
    </>
  )
}

export default TextArea
